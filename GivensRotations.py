import math

import numpy as np


def transposition(n, i, j):
    trans = np.identity(n)
    trans[[i, j]] = trans[[j, i]]
    return trans


def norm(x):
    return sum(i ** 2 for i in x) ** 0.5

def givens(x, y):
    square = math.sqrt(x ** 2 + y ** 2)
    cos = 1.0 * x / square
    sin = 1.0 * y / square

    return np.array([[cos, sin],
                      [-sin, cos]])

def givens_rotation(a, tolerance=1e-16):
    rank = np.linalg.matrix_rank(a)
    m = len(a)
    n = len(a[0])
    q = np.identity(m)
    P = np.identity(n)
    for j in range(n):
        if norm(a[j:, j:].T[n - j - 1]) < tolerance:
            trans = transposition(n, j, n-j-1)
            a[:] = np.dot(a, trans)
            P[:] = np.dot(trans, P)
        for i in range(m-1, j, -1):
            givens_matrix = givens(a[j, j].copy(), a[i, j].copy())
            a[[j, i], j:n] = np.dot(givens_matrix, a[[j, i], j:n].copy())
            q[[j, i], :] = np.dot(givens_matrix, q[[j, i], :].copy())

    return np.linalg.inv(P), a, q.T, rank

