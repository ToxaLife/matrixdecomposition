#!/usr/bin/env python
import sys

if sys.version_info[0] != 3:
    print("This script requires Python version 3")
    sys.exit(1)

import numpy as np
import Tests as test

np.set_printoptions(suppress=False, linewidth=160, precision=17)


def main():

    test_num = 6
    m = 6
    n = 3
    flag = input()

    test.test(test_num, n, m, flag)


if __name__ == '__main__':
    main()
