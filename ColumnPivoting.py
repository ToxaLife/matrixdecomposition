import numpy as np


def make_p(p):
    n = len(p)
    pm = []
    for i in range (0, n):
        pmatr = np.identity(n)
        temp = pmatr[i].copy()
        pmatr[i] = pmatr[p[i]]
        pmatr[p[i]] = temp
        pm.append(pmatr)

    result = np.identity(n)
    for elem in pm:
        result = np.dot(result, elem)

    return result


def make_q(a, r):
    m = len(a)
    q = np.identity(m, dtype=float)
    v = [0 * i for i in range(0, m)]

    for j in range(r-1, -1, -1):
        v[j] = 1
        v[j+1:m] = a[j+1:m, j]
        q[j:m, j:m] = row_house(q[j:m, j:m], v[j:m])

    return q


def make_r(a):
    for i in range(0, len(a[0])):
        a[i+1:len(a), i] = np.zeros(len(a) - (i+1))
    return a


def swap_a(a, r, k):
    temp = a[:, k].copy()
    a[:, k] = a[:, r]
    a[:, r] = temp

def norm(x):
    return sum(elem ** 2 for elem in x) ** 0.5

def house(x):
    n = len(x)
    nu = norm(x)
    v = x
    if nu != 0:
        b = x[0] + np.sign(x[0]) * nu
        v[1:n] = v[1:n] / b
    v[0] = 1
    return v


def row_house(a, v):
    beta = -2.0 / np.dot(np.transpose(v), v)
    vm = np.array([v]).transpose()
    w = beta * np.dot(a.transpose(), vm)
    a += np.dot(vm, w.transpose())
    return a

def column_pivoting(a):
    m = len(a)
    n = len(a[0])
    c = np.zeros(n)
    for j in range(0, n):
        for i in range(0, m):
            c[j] += np.transpose(a)[j][i] * a[i][j]

    r = 0
    k = 0
    t = c[n-1]

    for i in range(0, n):
        if c[i] == t:
            k = i
            break

    piv = np.zeros(n)
    while t > 0:
        v = np.zeros(m)
        piv[r] = k
        swap_a(a, r, k)
        c[k], c[r] = c[r], c[k]
        v[r:m] = house(a[r:m, r].copy())
        a[r:m, r:n] = row_house(a[r:m, r:n].copy(), v[r:m].copy())
        a[r+1:m, r] = v[r+1:m].copy()
        for i in range(r+1, n):
            c[i] -= a[r][i] ** 2
        if r < n - 1:
            t = c[r+1:n+1].max()
            for i in range(r+1, n):
                if c[i] == t:
                    k = i
                    break
        else:
            t = 0
        r += 1
    q = make_q(a, r)
    a = make_r(a)
    p = make_p(piv)
    return p, a, q, r


def compl_ort_decomp(a, r):
    a = a[:r].transpose()
    m, n = a.shape

    for i in range(0, m):
        v = np.zeros(m)
        v[i:n] = house(a[i:n, i].copy())

        a[i:n, i:m] = row_house(a[i:n, i:m].copy(), v[i:n])
        if i < n:
            a[i+1:n, i] = v[i+1:n]
    u = make_q(a.copy(), m)
    a = make_r(a.copy())

    return u, a.T