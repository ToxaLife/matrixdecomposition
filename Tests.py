import numpy as np

import ColumnPivoting as cp
import GivensRotations as gr


def test(test_num, n, m, flag):

    if flag.lower() == 'p':
        qr_function = cp.column_pivoting
    elif flag.lower() == 'g':
        qr_function = gr.givens_rotation
    else:
        return

    error = []
    errorCompleteDec = []
    errorLstSqr = []
    ident = np.identity(m)
    acc = 1e-15
    a = np.zeros((m, n))
    for i in range(0, test_num):
        a = np.random.randn(m, n)
        A = a.copy()
        print("-"*24*n + "TEST #" + str(i+1) + "-"*24*n)
        pmatr, r, q, rank = qr_function(a.copy())
        q_r_p = np.dot(np.dot(q, r), np.linalg.inv(pmatr))
        q_qt = np.dot(q, np.transpose(q))

        if (np.linalg.norm(q_qt - ident) < acc):
            print(">Matrix Q is ortog")
        else:
            print(">!!!! MATRIX Q IS NOT ORTOG")
            print(q_qt, end='\n\n')

        error.append(np.linalg.norm(q_r_p - a, 'fro'))
        print(">Error:" + str(error[i]))
        print("\n" + "\t"*5 + "Matrx A " + "\t"*6*n + "Q * R * inv(P)")
        for j in range(m):
            print(a[j], end='\t\t\t\t  \t')
            print(q_r_p[j])
        print("Q")
        print(q)
        print("R")
        print(r)
        print("P")
        print(pmatr)
        u, t = cp.compl_ort_decomp(r.copy(), rank)
        for j in range(m-rank):
            t = np.vstack((t, np.zeros(n)))
        print("U:")
        print(u)
        print("T")
        print(t)
        qta = np.dot(q.transpose(), a)
        print("T = Qt  A  P  U")
        u = np.dot(pmatr, u)
        t_check = np.dot(qta, u)
        print(t_check)
        print("Q T U*")
        qtu = np.dot(np.dot(q, t_check), u.T)
        errorCompleteDec.append(np.linalg.norm(t_check - t, 'fro'))
        print(">Error for cod:" + str(errorCompleteDec[i]))
        print("\n" + "\t"*5 + "Matrx A " + "\t"*6*n + "Q T U*")
        for j in range(m):
            print(a[j], end='\t\t\t\t  \t')
            print(qtu[j])

        b = np.random.randn(m)
        print("\nb")
        print(b)
        t_inv = np.linalg.inv(t[:rank, :rank])
        qt = q.transpose()
        cd = np.dot(qt, b)
        c = cd[:rank]
        if n > rank:
            y = np.vstack((np.dot(t_inv, c), np.zeros(n-rank)))
        else:
            y = np.dot(t_inv, c)
        x = np.array([np.dot(u, y)])

        print("X:")
        print(x)
        print("x_check")
        x_check = np.array([(np.linalg.lstsq(A, b))[0]])
        print(x_check)
        errorLstSqr.append(np.linalg.norm(x_check - x, 'fro'))

    print("\n\nAVG err:\t\t\t" + str(np.mean(error)))
    print("AVG err for cod:\t" + str(np.mean(errorCompleteDec)))
    print("AVG err for lst_sqr\t" + str(np.mean(errorLstSqr)))
